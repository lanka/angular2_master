
var basePath = 'app/frontend/';
var tsDir = 'ts';
var controllerDir = 'controller';
var jsDir = 'js';
var scssDir = 'scss';
var cssDir = 'css';


var gulp = require('gulp');

/* CSS */
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var precss = require('precss');
var cssnano = require('cssnano');
//var sass = require('gulp-scss');

/* JS & TS */
var jsuglify = require('gulp-uglify');
var ts = require('gulp-typescript');

var tsProject = ts.createProject('tsconfig.json');


gulp.task('ts-build',function(){
	return gulp.src(basePath+tsDir+'/*.ts')
	.pipe(sourcemaps.init())
	.pipe(ts(tsProject.compilerOptions))
	.pipe(sourcemaps.write())
	//.pipe(jsuglify())
	.pipe(gulp.dest(basePath+controllerDir))
})

gulp.task('scss-build',function(){
	return gulp.src(basePath+scssDir+'/*.scss')
	.pipe(sourcemaps.init())
	.pipe(postcss([precss,autoprefixer,cssnano]))
	//.pipe(sass())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(basePath+cssDir))
})

gulp.task('watch',['ts-build','scss-build'],function(){
	gulp.watch(basePath+tsDir+'/*.ts',['ts-build']);
	gulp.watch(basePath+scssDir+'/*.scss',['scss-build']);
})

gulp.task('default',['watch']);